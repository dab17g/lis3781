# LIS 3781 - Advanced Database Management

## Dathan Busby

### Assignment Links:

1. [A1 README.md](https://bitbucket.org/dab17g/lis3781/src/master/a1/README.md)
	- Learn VCS
    - Install AMMPS
    - Create lis3781 repository and provide read only access
    - Create BitBucketLocationStations repository
    - Provide Git command descriptions
    - Create ERD
	
2. [A2 README.md](https://bitbucket.org/dab17g/lis3781/src/master/a2/README.md)
    - Create users
    - Create customer and company table
    - populate tables
    - encrypt customer table values
	
3. [A3 README.md](https://bitbucket.org/dab17g/lis3781/src/master/a3/README.md)
    - Create three tables in Oracle
    - SQL orders
    - populate tables
    - upload screenshots of populated tables

4. A4 README.md

5. [A5 README.md](https://bitbucket.org/dab17g/lis3781/src/master/a5/README.md)
    - Create database using tables from a4 as well as new tables
    - Create time table
    - Create stored procedures
    - Complete a5 questions

6. [P1 README.md](https://bitbucket.org/dab17g/lis3781/src/master/p1/)
    - Create and engineer database
	- Create tables
	- Insert data into tables including hashed values
	- must forward engineer

7. P2 README.md

[bitbucketlocationstations link](https://bitbucket.org/dab17g/bitbucketstationlocations/src/master/)
	