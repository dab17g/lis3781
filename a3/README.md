# Assignment 3

### Assignment Requirements
- Create 3 tables with Oracle
- Insert values into tables
- Must forward engineer successfuly
- upload screenshots

![](https://bitbucket.org/dab17g/lis3781/raw/bff054009354c134a587c06f6e89cfe0f25563d3/a3/img/comm.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/bff054009354c134a587c06f6e89cfe0f25563d3/a3/img/cust.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/bff054009354c134a587c06f6e89cfe0f25563d3/a3/img/ord.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/1fa01f448a5a0c22cd08179eaffd22b102fdab7f/a3/img/sql_1.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/1fa01f448a5a0c22cd08179eaffd22b102fdab7f/a3/img/sql_2.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/1fa01f448a5a0c22cd08179eaffd22b102fdab7f/a3/img/sql_3.PNG)