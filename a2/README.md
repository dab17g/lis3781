# Assignment 2

### Assignment Requirements
- Create users
- Create customer and company table
- populate tables
- encrypt customer table values

![](https://bitbucket.org/dab17g/lis3781/raw/f7e6d1db13895eaa9eee62a1e41fd542030bf866/a2/sql_code/sql_a.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/f7e6d1db13895eaa9eee62a1e41fd542030bf866/a2/sql_code/sql_b.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/f7e6d1db13895eaa9eee62a1e41fd542030bf866/a2/sql_code/sql_c.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/f7e6d1db13895eaa9eee62a1e41fd542030bf866/a2/sql_code/tables.PNG)
