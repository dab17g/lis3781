# Project 1

### Assignment Requirements
- Create and engineer database
- Create tables
- Insert data into tables including hashed values
- must forward engineer

Solutions file contains all SQL code. It is located in files folder in p1

[Link to p1 MWB](https://bitbucket.org/dab17g/lis3781/src/master/p1/files/p1.mwb)

[Link to solutions file](https://bitbucket.org/dab17g/lis3781/src/master/p1/files/lis3781_p1_solutions.sql)

![](https://bitbucket.org/dab17g/lis3781/raw/4280f2127ff0d9669a6ea8dcfc6266fbe06626c3/p1/files/erd.png)