# Assignment 5

### Assignment Requirements
- Create database using tables from a4 as well as new tables
- Create time table
- Create stored procedures
- Complete a5 questions

[a5.sql link](https://bitbucket.org/dab17g/lis3781/src/4b66bbce0da9/a5/a5.sql?at=master)

![](https://bitbucket.org/dab17g/lis3781/raw/4b66bbce0da933a708ed341aebf8e9bafc296b2b/a5/tabbea5.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/4b66bbce0da933a708ed341aebf8e9bafc296b2b/a5/screenshots/1.PNG)
![](https://bitbucket.org/dab17g/lis3781/raw/4b66bbce0da933a708ed341aebf8e9bafc296b2b/a5/screenshots/2.PNG)

