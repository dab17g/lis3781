SET ANSI_WARNINGS ON;
GO

use master;
GO

if exists (select name from master.dbo.sysdatabases where name = N'dab17g')
drop database dab17g;
GO

if not exists (select name from master.dbo.sysdatabases where name = N'dab17g')
create database dab17g;
GO

use dab17g;
GO

if OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
drop table dbo.person;
GO

create table dbo.person
(
    per_id smallint not null identity(1,1),
    per_ssn binary(64) null,
	per_salt binary(64) null,
    per_fname varchar(15) not null,
    per_lname varchar(30) not null,
    per_gender CHAR(1) not null check (per_gender in('m', 'f')),
    per_dob date not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null default 'FL',
    per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email varchar(100) null,
    per_type char(1) not null check (per_type in('c', 's')),
    per_notes varchar(45) null,
    primary key(per_id),

    constraint ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

if object_id (N'dbo.phone', N'U') is not NULL
drop table dbo.phone;
GO
create table dbo.phone
(
    phn_id smallint not null identity(1,1),
    per_id smallint not null,
    phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) not null check (phn_type in('h','c','w','f')),
    phn_notes varchar(255) null,
    primary key (phn_id),

    constraint fk_phone_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    on delete CASCADE
    on update CASCADE
);

if object_id (N'dbo.customer', N'U') is not NULL
drop table dbo.customer;
GO
create table dbo.customer
(
    per_id smallint not null,
    cus_balance decimal(7,2) not null check (cus_balance >= 0),
    cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
    cus_notes varchar(45) null,
    primary key (per_id),

    CONSTRAINT fk_customer_person
    FOREIGN key (per_id)
    REFERENCES dbo.person(per_id)
    on delete CASCADE
    on update CASCADE
);

if object_id (N'dbo.slsrep', N'U') is not NULL
drop table dbo.slsrep;
GO
create table dbo.slsrep
(
    per_id smallint not null,
    srp_yr_sales_goal decimal(8,2) not null check(srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
    srp_notes varchar(45) null,
    primary key (per_id),

    CONSTRAINT fk_slsrep_person
    FOREIGN key (per_id)
    REFERENCES dbo.person(per_id)
    on delete CASCADE
    on update CASCADE
);

if object_id (N'dbo.srp_hist', N'U') is not NULL
drop table dbo.srp_hist;
GO
create table dbo.srp_hist
(
    sht_id smallint not null identity(1,1),
    per_id smallint not null,
    sht_type char(1) not null check (sht_type in('i','u','d')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
    sht_notes varchar(45) null,
    primary key(sht_id),

    CONSTRAINT fk_srp_hist_slsrep
    FOREIGN key (per_id)
    REFERENCES dbo.slsrep(per_id)
    on delete CASCADE
    on update CASCADE
);

if object_id (N'dbo.contact', N'U') is not NULL
drop table dbo.contact;
GO
create table dbo.contact
(
    cnt_id int not null identity(1,1),
    per_cid smallint not null,
    per_sid smallint not null,
    cnt_date datetime not null,
    cnt_notes varchar(255) null,
    primary key(cnt_id),

    CONSTRAINT fk_contact_customer
    FOREIGN key (per_cid)
    REFERENCES dbo.customer(per_id)
    on delete CASCADE
    on update cascade,

    CONSTRAINT fk_contact_slsrep
    FOREIGN key (per_sid)
    REFERENCES dbo.slsrep(per_id)
    on delete no ACTION
    on update no action
);

if object_id (N'dbo.[order]', N'U') is not null
drop table dbo.[order];
GO

create table dbo.[order]
(
    ord_id int not null identity(1,1),
    cnt_id int not null,
    ord_placed_date datetime not null,
    ord_filled_date datetime null,
    ord_notes varchar(255) null,
    primary key (ord_id),

    constraint fk_order_contact
    foreign key (cnt_id)
    references dbo.contact (cnt_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.region', N'U') is not null
drop table dbo.region;
GO

create table dbo.region
(
    reg_id tinyint not null identity(1,1),
    reg_name char(1) not null,
    reg_notes varchar(255) null,
    primary key (reg_id)
);
GO

if object_id (N'dbo.state', N'U') is not null
drop table dbo.state;
GO

create table dbo.state
(
    ste_id tinyint not null identity(1,1),
    reg_id tinyint not null,
    ste_name char(2) not null default 'FL',
    ste_notes varchar(255) null,
    primary key (ste_id),

    constraint fk_state_region
    foreign key (reg_id)
    references dbo.region (reg_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.city', N'U') is not null
drop table dbo.city;
GO

create table dbo.city
(
    cty_id smallint not null identity(1,1),
    ste_id tinyint not null,
    cty_name varchar(30) not null,
    cty_notes varchar(255) null,
    primary key (cty_id),

    constraint fk_city_state
    foreign key (ste_id)
    references dbo.state (ste_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.store', N'U') is not null
drop table dbo.store;
GO

create table dbo.store
(
    str_id smallint not null identity(1,1),
    cty_id smallint not null,
    str_name varchar(45) not null,
    str_street varchar(30) not null,
	str_city varchar(30) not null,
	str_state char(2) not null,
    str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint not null check(str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email varchar(100) not null,
    str_url varchar(100) not null,
    str_notes varchar(255) null,
    primary key (str_id),

    constraint fk_store_city
    foreign key (cty_id)
    references dbo.city (cty_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
GO

create table dbo.invoice
(
    inv_id int not null identity(1,1),
    ord_id int not null,
    str_id smallint not null,
    inv_date datetime not null,
    inv_total decimal(8,2) not null check (inv_total >= 0),
    inv_paid bit not null,
    inv_notes varchar(255) null,
    primary key (inv_id),

    constraint fk_invoice_order
    foreign key (ord_id)
    references dbo.[order] (ord_id)
    on delete cascade
    on update cascade,

    constraint fk_invoice_store
    foreign key (str_id)
    references dbo.store (str_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.payment', N'U') is not null
drop table dbo.payment;
GO

create table dbo.payment
(
    pay_id int not null identity(1,1),
    inv_id int not null,
    pay_date datetime not null,
    pay_amt decimal(7,2) not null check (pay_amt >= 0),
    pay_notes varchar(255) null,
    primary key (pay_id),

    constraint fk_payment_invoice
    foreign key (inv_id)
    references dbo.invoice (inv_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
GO

create table dbo.vendor
(
    ven_id smallint not null identity(1,1),
    ven_name varchar(45) not null,
    ven_street varchar(30) not null,
    ven_city varchar(30) not null,
    ven_state char(2) not null default 'FL',
    ven_zip int not null,
    ven_phone bigint not null,
    ven_email varchar(100) null,
    ven_url varchar(100) null,
    ven_notes varchar(100) null,
    primary key (ven_id)
);
GO

if object_id (N'dbo.product', N'U') is not null
drop table dbo.product;
GO

create table dbo.product
(
    pro_id smallint not null identity(1,1),
    ven_id smallint not null,
    pro_name varchar(30) not null,
    pro_descript varchar(45) null,
    pro_weight float not null check (pro_weight >= 0),
    pro_qoh smallint not null check (pro_qoh >= 0),
    pro_cost decimal(7,2) not null check (pro_cost >= 0),
    pro_price decimal(7,2) not null check (pro_price >= 0),
    pro_discount decimal(3,0) null,
    pro_notes varchar(255) null,
    primary key (pro_id),

    constraint fk_product_vendor 
    foreign key (ven_id)
    references dbo.vendor(ven_id)
    on delete cascade
    on update cascade
);
GO

if object_id (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
GO

create table dbo.product_hist
(
    pht_id int not null identity(1,1),
    pro_id smallint not null,
    pht_date datetime not null,
    pht_cost decimal(7,2) not null check (pht_cost >= 0),
    pht_price decimal(7,2) not null check (pht_price >= 0),
    pht_discount decimal(3,0) null,
    pht_notes varchar(255) null,
    primary key(pht_id),

    constraint fk_product_hist_product
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade 
    on update cascade
);
GO

if object_id (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
GO

create table dbo.order_line
(
    oin_id int not null identity(1,1),
    ord_id int not null,
    pro_id smallint not null,
    oin_qty smallint not null check (oin_qty >= 0),
    oin_price decimal(7,2) not null check (oin_price >= 0),
    oin_notes varchar(255) null,
    primary key(oin_id),

    Constraint fk_order_line_order
    Foreign key (ord_id)
    References dbo.[order] (ord_id)
    on delete cascade
    on update cascade,

    Constraint fk_order_line_product
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade
    on update cascade
);
GO



if object_id (N'dbo.time', N'U') is not null
drop table dbo.time;
GO

create table dbo.time
(
    tim_id int not null identity(1,1),
    tim_yr smallint not null, 
    tim_qtr tinyint not null,
    tim_month tinyint not null,
    tim_week tinyint  not null,
    tim_day tinyint not null,
    tim_time time not null,
    tim_notes varchar(255) null,
    primary key(tim_id)
);
GO

if object_id (N'dbo.sale', N'U') is not null
drop table dbo.sale;
GO

create table dbo.sale
(
    pro_id smallint not null,
    str_id smallint not null,
    cnt_id int not null,
    tim_id int not null,
    sal_qty smallint not null,
    sal_price decimal(8,2) not null,
    sal_total decimal(8,2) not null,
    sal_notes varchar(255) null,
    primary key (pro_id, cnt_id, tim_id, str_id),

    constraint ux_pro_id_str_id_cnt_id_tim_id
    unique nonclustered (pro_id asc, str_id asc, cnt_id asc, tim_id asc),

    constraint fk_sale_time 
    foreign key (tim_id)
    references dbo.time(tim_id)
    on delete cascade
    on update cascade,

    constraint sale_contract
    foreign key (cnt_id)
    references dbo.contact (cnt_id)
    on delete cascade 
    on update cascade,

    constraint sale_store
    foreign key (str_id)
    references dbo.store (str_id)
    on delete cascade
    on update cascade,

    constraint fk_sale_product 
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade
    on update cascade
);
GO

select * from information_schema.tables;

select HASHBYTES('sha2_512', 'test');

select len(HASHBYTES('SHA2_512', 'test'));

INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve', 'Rogers', 'm', '2000-01-01', '437 Southern Drive','Rochester','NY', 323444444, 'srogers@comcast.net', 'c',null),
(2, null, 'Bruce','Wayne', 'm', '2000-01-01','1007 Mountain Drive', 'Gotham', 'NY', 323444444,'bwayne@knology.net','c',null),
(3, null, 'Peter','Parker', 'm', '1976-09-10', '20 Ingram Street', 'New York', 'NY', 323444444,'pparker@ms.com','c',null),
(4, null, 'Jane','Thompson','m', '1950-01-10', '13563 Ocean View Drive', 'Seattle','WA',323444444,'jthompson@gmail.com','c',null),
(5, null, 'Debra','Steele', 'm', '1976-09-10', '543 Oak Ln','Milwaukee','WI',323444444,'dsteele@verizon.net','c',null),
(6, null, 'Tony', 'Stark', 'm', '1950-01-10', '332 Palm Avenue', 'Mailbu', 'CA',323444444,'tsark@yahoo.com','s',NUll),
(7, null, 'Hank','Pymi', 'm', '2000-01-01','2355 Brown Street','Cleveland','OH',323444444,'hyopm@aol.com','s',null),
(8, null, 'Bob','Best', 'm', '1976-09-10', '4902 Avendal Avenue', 'Scottsdale','AZ',323444444,'bbest@yahoo.com','s',null),
(9, null, 'Sandra','Dole', 'f', '1950-01-10', '87912 Thunderbird Ln', 'Sioux Falls', 'SD',323444444,'bavery@gmail.com','s',null),
(10, null, 'Ben','Avery', 'f', '2000-01-01','6432 Lawrence Ave','Atlanta', 'GA',323444444,'sdole@gmail.com','s',null),
(11, null, 'Arthur','Curry', 'f', '1950-01-10', '3304 Euclid Avenue', 'Miami', 'FL',323444444,'acurry@gmailcom','s',null),
(12, null, 'Diana','Price', 'f', '1976-09-10', '944 Green Street', 'Las Vegas', 'NV',323444444,'dprice@hotmail.com','s',null),
(13, null, 'Adam','Jurris', 'f', '2000-01-01','98435 Valencia Dr','Gulf Shores','AL',323444444,'ajurris@gmx.com','s',null),
(14, null, 'Judy','Sleen','f', '1976-09-10', '56343 Rover Ct.','Billings','MT',323444444, 'jsleen@sympatico.com','s',null),
(15, null, 'Bill','Neiderheim','f', '2000-01-01','43567 Netherland Blvd','South Bend','IN',323444444,'bneiderheim@comcast.net','c', null);
GO
select * from dbo.person;

insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, null),
(2, 80000, 35000, 9650, null),
(3, 150000, 84000, 3500, null),
(4, 98000, 43000, 8750, null),
(5, 125000, 87000, 1530, null);

select * from dbo.slsrep;

insert into dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES 
(6, 120, 14789, null),
(7, 98.43, 234.92, null),
(8, 0, 4578, 'customer always on time'),
(9, 981.47, 1672.38, 'High Balnace'),
(10, 320, 782.90, null),
(11, 251.01, 13782.96, null),
(12, 582.76, 961.13, null),
(13, 121.78, 1057.45, 'Recent CUstomer'),
(14, 765.43, 6789.42, 'Buys bulk'),
(15, 304.39, 457.81, 'not recent');

select * from dbo.customer;

insert into dbo.contact 
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1, 6, '1999-01-01', null),
(2, 6, '2001-09-29', null),
(3, 7, '2002-08-19', null),
(2, 7, '2002-09-01', null),
(4, 7, '2004-02-01', null),
(5, 8, '2004-03-01', null),
(4, 8, '2004-07-08', null),
(1, 9, '2005-05-03', null),
(5,9,'2005-06-21',null),
(3,11, '2005-09-08',null),
(4, 13, '2006-04-15', null),
(2, 15, '2006-08-04', null);

select * from dbo.contact;

insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-23', null),
(2, '2005-03-19', '2005-07-25', null),
(3, '2011-07-01', '2011-07-06', null),
(4, '2009-12-23', '2010-01-01', null),
(5, '2008-09-21', '2008-09-27', null), 
(6, '2009-04-21', '2009-05-10', null),
(7, '2010-05-31', '2010-06-04', null),
(8, '2007-09-02', '2007-11-03', null),
(9, '2011-01-01', '2011-02-02', null),
(10, '2012-02-29', '2012-05-09',null);

select * from dbo.[order];

insert into region
(reg_name, reg_notes)
VALUES
('c', null),
('n', null),
('e', null),
('s', null),
('w', null);
GO

select * from dbo.region;
insert into state
(reg_id, ste_name, ste_notes)
values
(1, 'MI', null),
(3, 'IL', null),
(4, 'WA', null),
(5, 'FL', null),
(2, 'TX', null);
GO
select * from dbo.state;

insert into city 
(ste_id, cty_name, cty_notes)
values
(1, 'Detroit', null),
(2, 'Aspen', null),
(2, 'Chicago', null),
(3, 'Clover', null),
(4, 'St.louis', null);
GO

select * from city;



insert into dbo.store
(cty_id, str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
(2, 'Walgreens', '1232 Walnut Ln', 'Aspen', 'IL', '475315690', '3127658127', 'info@walgreens.com', 'walgreens.com', null),
(3, 'CVS', '777 Casrper Rd', 'CHicago', 'IL', '505231516', '3128926534', 'help@cvs.com', 'cvs.com', null),
(4, 'Lowes', '811309 Cataplut Ave', 'Clover', 'WA', '802345671', '9017654321', 'sales@lowes.com', 'lowes.com', null),
(5, 'Walmart', '14567 Greek Grove', 'St. Louis', 'FL', '222444777', '4567851245', 'info@walmart.com','walmart.com',null),
(1, 'Dollar General', '3242 Davison Road', 'DEtroit', 'MI', '145698598', '4575632104', 'ask@dolargeneral.com', 'dollargeneral.com', null);

select * from dbo.store;

insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5, 1, '2001-05-03', 58.32, 0, null),
(4,1, '2006-11-11', 100.89, 0, null),
(1,1, '2010-09-24', 57.34, 0, null),
(3,2, '2011-01-10', 99.32, 1, null),
(2,3, '2008-06-05', 1109.27, 1, null),
(6,4, '2009-04-20', 239.83, 0, null),
(7,5, '2010-05-05', 537.29, 0, null),
(8,2, '2007-09-09', 644.21, 1, null),
(9,3, '2011-12-17', 934.21, 1, null),
(10,4, '2012-03-18', 27.33, 0, null);

select * from dbo.invoice;

insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '333 Dolphin Run', 'Orlando', 'FL', '256321024', '0000000000', 'sysco@gmail.com', 'sysco.com', null),
('General ELectric', '100 Happy Trails', 'Boston', 'MA', '1111111111','4568521021', 'support@GE.com', 'GE.com', null),
('Cisco', '300 Cisco Dr', 'Stanford', 'OR', '400023565', '2222222222', 'cisco@cisco@gmail.com', 'cisco.com', null),
('Goodyear', '100 Goodyear Dr', 'Gary', 'IN', '458987859', '3333333333', 'sales@goodyear.com', 'goodyear.com', null),
('Snap-On', '4223 Magenta Ave', 'Lake Flass', 'ND', '475896321', '4444444444', 'support@snapon.com', 'snapon.com', null);

select * from dbo.vendor;

insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', '', 2.4, 45, 4.99, 7.96, 30, 'Discounted when purchase combo'),
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, null, null),
(3, 'pail', '16 gallon', 2.8, 48, 3.89, 7.99, 40, null),
(4, 'cooking oil', 'Peanut oil', 15, 19, 19.99, 28.99, null, 'gallons'),
(5, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale.');

select * from dbo.product;

insert into dbo.order_line
(ord_id, pro_id, oin_qty, oin_price, oin_notes)
VALUES
(1,2,10,8.0,null),
(2, 3, 7, 9.88, null),
(3, 4, 3, 6.99, null),
(5,1, 2, 12.77, null),
(4, 5, 13, 58.99, null);

select * from dbo.order_line;

insert into dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5, '2008-07-01', 5.99, null),
(4, '2012-09-28', 4.99, null),
(1, '2008-07-23', 8.75, null),
(3, '2010-10-31', 19.55, null),
(2, '2011-03-29', 32.5, null),
(6, '2010-10-03', 20.00, null),
(8, '2008-08-08', 1000.00, null),
(9, '2009-01-10', 103.88, null),
(7, '2007-03-15', 25.00, null),
(10, '2007-05-12', 40.00, null),
(4, '2007-05-22', 9.33, null);

select * from dbo.payment;

insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchsed with screwdriver set'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, null, null),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, null),
(4, '2005-05-06 18:09:04', 19.99, 28.99, null, 'gallons'),
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, '1/2 price');

select * from dbo.product_hist;

insert into time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
values
(2008, 2, 5, 19, 7, '11:59:59', null),
(2010,4, 12, 49, 4, '08:34:21', null),
(1999, 4, 12, 52, 5, '05:21:34', null),
(2011, 3, 8, 36, 1, '09:32:18', null),
(2001, 3, 7, 27, 2, '23:56:32', null),
(2008, 1,1, 5, 4, '04:22:36', null),
(2010, 2, 4, 14, 5, '02:49:11', null),
(2014, 1, 2, 8, 2, '12:27:14', null),
(2013, 3, 9, 38, 4, '10:12:28', null),
(2012, 4, 11, 47, 3, '22:36:22', null),
(2014, 2, 6, 23, 3, '19:07:10', null);
GO

select * from dbo.time;

insert into sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
values
(1, 5, 5, 3, 20, 9.99, 199.8, null),
(2, 4, 6, 2, 5, 5.99, 29.95, null),
(3, 3, 4, 1, 30, 3.99, 119.7, null),
(4, 2, 1, 5, 15, 18.99, 284.40, null),
(5, 1, 2, 4, 6, 11.99, 71.94, null),
(5, 2, 5, 6, 10, 9.99, 199.8, null),
(4, 3, 6, 7, 5, 5.99, 29.95, null),
(3, 1, 4, 8, 30, 3.99, 119.7, null),
(2, 3, 1, 9, 15, 18.99, 284.30, null),
(1, 4, 2, 10, 6, 11.99, 71.94, null),
(1, 2, 3, 11, 10, 11.99, 119.9, null);
GO

select * from dbo.sale;

insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), system_user, getDate(), 100000, 110000, 11000, null),
(4, 'i', getDate(), system_user, getDate(), 150000, 17000, 11000, null),
(3, 'u', getDate(), system_user, getDate(), 200000, 185000, 11000, null),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 10000, null),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, null);

select * from dbo.srp_hist;

insert into dbo.phone
(per_id, phn_num, phn_type, phn_notes)
values
(2, 8500001023, 'c', 'never answers'),
(5, 7778889999, 'w', 'shut upppsss'), 
(8, 8400209888, 'h', null),
(10, 8283838343, 'f', 'boring'), 
(14, 9999292922, 'c', 'best');
GO

select * from dbo.phone;



CREATE PROC dbo.CreatePersonSSN
AS
BEGIN

    declare @salt binary(64);
    declare @ran_num int;
    declare @ssn binary(64);
    declare @x int, @y int;
    set @x = 1;

    set @y = (select count(*) from dbo.person);
        while (@x <= @y)
        BEGIN
        SET @salt = CRYPT_GEN_RANDOM(64);
        set @ran_num = FLOOR(RAND()*(999999999 - 111111111 + 1)) + 111111111;
        set @ssn = HASHBYTES('SHA2_512', concat(@salt, @ran_num));

        update dbo.person
        set per_ssn = @ssn, per_salt=@salt
        where per_id = @x;

        set @x = @x + 1;

        end;
end;
GO

exec dbo.CreatePersonSSN