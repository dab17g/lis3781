# Assignment 1

### Assignment Requirements
- Learn VCS
- Install AMMPS
- Create lis3781 repository and provide read only access
- Create BitBucketLocationStations repository
- Provide Git command descriptions
- Create ERD

### Git command descriptions:
1. git innit: used to initialize a local directory to become a git repository. The first thing you should do when making a repository.
2. git status: Displays the status of the directory. Also shows the files in the staging area.
3. git add: adds the files that have been changed or added to the staging area.
4. git commmit: Takes a screenshot of the files/changes in the staging area.
5. git push: Pushes the commit to the remote repository.
6. git pull: Downloads the files/changes that are present in the remote repository but not the local repository to the local repository.
7. git clone: Used to clone a remote repository to a local directory.

[Bit bucket station locations](https://bitbucket.org/dab17g/bitbucketstationlocations/src/master/)

![](https://bitbucket.org/dab17g/lis3781/raw/fedd337ecb90875442d58453c4d86d57b521b291/a1/final_erd.png)
![](https://bitbucket.org/dab17g/lis3781/raw/9843f1a3abd0f33fc51ef59f5f4ed41363c37466/a1/ampps.PNG)
